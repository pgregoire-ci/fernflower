# fernflower

Daily build of the following:
https://github.com/JetBrains/intellij-community/tree/master/plugins/java-decompiler/engine


## Direct artifact link

https://gitlab.com/pgregoire-ci/fernflower/-/jobs/artifacts/main/raw/fernflower.jar?job=build
